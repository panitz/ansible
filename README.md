# Ansible CLI #

This is a Docker image for the [Ansible CLI](https://docs.ansible.com/). You can use it to use Ansible on your linux/AMD64
system to run the Ansible components without installing ansible on the machine.

**NOTE:** This image is based on the latest [debian:buster-slim](https://hub.docker.com/_/debian) image.
So the Ansible version is the version used in the base image! The image tag denotes the time the image
was built.

## Tags ##

- `latest` - This corresponds to the latest image.
- `<YYYYMMDD>` - Contains the image from the corresponding date.

## Usage ##

Create the following aliases to use Ansible as it would have been installed on your local system:

```shell
alias ansible='docker run --rm -it -v ~/.ssh:/root/.ssh:ro -v $(pwd):/ansible-workdir panitz/ansible:latest'
alias ansible-config='docker run --rm -it -v ~/.ssh:/root/.ssh:ro -v $(pwd):/ansible-workdir --entrypoint=/usr/bin/ansible-config panitz/ansible:latest'
alias ansible-connection='docker run --rm -it -v ~/.ssh:/root/.ssh:ro -v $(pwd):/ansible-workdir --entrypoint=/usr/bin/ansible-connection panitz/ansible:latest'
alias ansible-console='docker run --rm -it -v ~/.ssh:/root/.ssh:ro -v $(pwd):/ansible-workdir --entrypoint=/usr/bin/ansible-console panitz/ansible:latest'
alias ansible-doc='docker run --rm -it -v ~/.ssh:/root/.ssh:ro -v $(pwd):/ansible-workdir --entrypoint=/usr/bin/ansible-doc panitz/ansible:latest'
alias ansible-galaxy='docker run --rm -it -v ~/.ssh:/root/.ssh:ro -v $(pwd):/ansible-workdir --entrypoint=/usr/bin/ansible-galaxy panitz/ansible:latest'
alias ansible-inventory='docker run --rm -it -v ~/.ssh:/root/.ssh:ro -v $(pwd):/ansible-workdir --entrypoint=/usr/bin/ansible-inventory panitz/ansible:latest'
alias ansible-playbook='docker run --rm -it -v ~/.ssh:/root/.ssh:ro -v $(pwd):/ansible-workdir --entrypoint=/usr/bin/ansible-playbook panitz/ansible:latest'
alias ansible-pull='docker run --rm -it -v ~/.ssh:/root/.ssh:ro -v $(pwd):/ansible-workdir --entrypoint=/usr/bin/ansible-pull panitz/ansible:latest'
alias ansible-test='docker run --rm -it -v ~/.ssh:/root/.ssh:ro -v $(pwd):/ansible-workdir --entrypoint=/usr/bin/ansible-test panitz/ansible:latest'
alias ansible-vault='docker run --rm -it -v ~/.ssh:/root/.ssh:ro -v $(pwd):/ansible-workdir --entrypoint=/usr/bin/ansible-vault panitz/ansible:latest'
```

These aliases mounts your .ssh and the current directory into the container.

You can now execute ansible like this:

```shell
$ ansible -i hosts all -l server-123 -a "bash -c 'cat /etc/issue*'"
```