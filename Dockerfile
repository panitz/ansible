FROM debian:buster-slim

RUN apt-get update \
    && apt-get install --no-install-recommends -y \
        ansible \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

WORKDIR /ansible-workdir
ENV PYTHONWARNINGS=ignore::UserWarning

ENTRYPOINT [ "/usr/bin/ansible" ]